<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class ApiValidationSubscriber implements EventSubscriberInterface
{
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (! $event->isMasterRequest()) {
            return;
        }

        $request = $event->getRequest();

        //no validation needed on "safe" HTTP methods like GET for example
        if ($request->isMethodSafe(false)) {
            return;
        }

        dd($request->attributes->all());

        if (! $request->attributes->get('_is_api')) {
            return;
        }

        if ($request->headers->get('Content-Type') != 'application/json') {
            $response = $this->json(array('message' => 'Invalid Content-Type'), 415);

            $event->setResponse($response);
            return;
        }

    }

    public static function getSubscribedEvents()
    {
        return [
           'kernel.request' => 'onKernelRequest',
        ];
    }
}
