<?php

namespace App\Controller;

use App\Api\ApiRoute;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @ApiRoute()
 */
class TestController extends AbstractController
{
    /**
     * @Route("/test", name="test", methods={"POST"})
     */
    public function index()
    {
        return $this->render('story.html.twig', []);
    }
}
