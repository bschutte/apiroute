<?php

namespace App\Controller;

use App\Api\ApiRoute;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class StoryController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('story.html.twig', []);
    }

    /**
     * @ApiRoute()
     * @Route("/stories/{slug}", name="stories_slug_set", methods={"POST"})
     */
    public function setStory()
    {
        return $this->render('story.html.twig', []);
    }
}
